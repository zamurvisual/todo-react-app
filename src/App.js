import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Todos from './components/Todo/Todos';
import AddTodo from './components/Todo/AddTodo';
import About from './components/pages/About';
import Header from './components/layout/Header';
// import uuid from 'uuid';
import axios from 'axios';
import './App.css';


class App extends Component {
  state = {
    todos: []
  }

  componentDidMount() {
    axios.get('https://jsonplaceholder.typicode.com/todos?_limit=6')
      .then(res => this.setState({ todos: res.data }))
  }  

  // Toggle complete
  markComplete = (id) => {
    this.setState ({
      todos: this.state.todos.map((todo) => {
        if (todo.id === id) {
          todo.completed = !todo.completed
        }
        return todo;
      })
    })
  }

  // Delete item
  deleteItem = (id) => {
    axios.delete(`https://jsonplaceholder.typicode.com/todos/${id}`)
      .then(res => this.setState({ 
        todos: [...this.state.todos.filter(todo => todo.id !== id)] 
      }));

    // this.setState({
    //   todos: [...this.state.todos.filter(todo => todo.id !== id )]
    // });
  }


  // Add to do
  addTodo = (title) => {

    // const newTodo = {
    //   id: uuid.v4(),
    //   title,
    //   completed: false
    // }

    // this.setState({
    //   todos: [...this.state.todos, newTodo]
    // })
    
    axios.post('https://jsonplaceholder.typicode.com/todos', {
      title,
      completed: false
    }).then(res => this.setState({
      todos: [...this.state.todos, res.data]
    }))
  }

  render() {
    return (
      <Router>
        <div className="App">
          <div className="container">
            <Header />

            <Route exact path="/" render={props => (
              <React.Fragment>
                <AddTodo addTodo={this.addTodo} />
                <Todos todos={this.state.todos} markComplete={this.markComplete} deleteItem={this.deleteItem} />
              </React.Fragment>
            )} />

            <Route path="/about" component={About} />

          </div>
        </div>
      </Router>
    );
  }
  
}

export default App;
