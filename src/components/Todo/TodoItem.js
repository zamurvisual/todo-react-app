import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class TodoItem extends Component {

    // Set todo item styles
    setStyle = () => {
        return {
            background: '#f4f4f4',
            padding: '10px',
            borderBottom: '1px #ccc dotted',
            textDecoration: this.props.todo.completed ? 'line-through' : 'none'
        }
    }

    render() {

        const { id, title } = this.props.todo;

        return (
            <div style={ this.setStyle() }>
                <p>
                    <input type="checkbox" onChange={this.props.markComplete.bind(this, id) } /> {' '}
                    { title }
                    <button style={btnStyle} onClick={this.props.deleteItem.bind(this, id)} >X</button>
                </p>
            </div>
        )
    }
}

// PropTypes
TodoItem.propTypes = {
    todo: PropTypes.object.isRequired,
    markComplete: PropTypes.func.isRequired,
    deleteItem: PropTypes.func.isRequired
}

// Styles
const btnStyle = {
    background: '#ff0000',
    color: '#fff',
    border: 'none',
    padding: '5px 8px',
    borderRadius: '50%',
    cursor: 'pointer',
    float: 'right',
}
