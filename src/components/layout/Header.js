import React from 'react';
import { Link } from 'react-router-dom';

export default function Header() {
    return (
        <header style={headerStyle} >
            <h1>TodoList</h1>
            <Link className='link' to="/">Home</Link> | <Link className='link' to="/about">About</Link>
        </header>
    )
}

// Styles
const headerStyle = {
    padding: '6px',
    background: '#333',
    textAlign: 'center',
    color: '#f2f2f2'
}